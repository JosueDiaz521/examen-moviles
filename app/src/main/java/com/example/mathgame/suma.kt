package com.example.mathgame

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import kotlin.random.Random


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [suma.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [suma.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class suma : Fragment() {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var gameScoreTextView: TextView
    internal lateinit var gameRoundTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var num1TextView: TextView
    internal lateinit var num2TextView: TextView
    internal lateinit var goButton: Button
    internal lateinit var resultado: TextView

    @State
    var score = 0
    var round = 1

    var num1 = Random.nextInt(0,10)
    var num2 = Random.nextInt(0,10)

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    @State
    var timeLeft = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_suma, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this, savedInstanceState)

        gameRoundTextView = view.findViewById(R.id.round)
        gameScoreTextView = view.findViewById(R.id.score)
        num1TextView = view.findViewById(R.id.num1)
        num2TextView = view.findViewById(R.id.num2)
        timeLeftTextView = view.findViewById(R.id.time)
        goButton = view.findViewById(R.id.sendButton)
        resultado = view.findViewById(R.id.Result)

        gameRoundTextView.text = getString(R.string.round, round)
        gameScoreTextView.text = getString(R.string.score, score)
        num1TextView.text = getString(R.string.Num1,num1)
        num2TextView.text = getString(R.string.Num2,num2)

        restoreGame()
    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score, score)
        gameRoundTextView.text = getString(R.string.round, round)
        num1 = Random.nextInt(0,10)
        num2 = Random.nextInt(0,10)
        num1TextView.text = getString(R.string.Num1,num1)
        num2TextView.text = getString(R.string.Num2,num2)
        timeLeft=10;

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time, timeLeft)
            }
        }
        countDownTimer.start()
        goButton.setOnClickListener { verificarResultado() }



    }

    private fun endGame(){
        Toast.makeText(activity, getString(R.string.end_game, score), Toast.LENGTH_LONG).show()
        gameScoreTextView.text = getString(R.string.score, score)
    }

    private fun verificarResultado(){
        var result = num1 + num2
        //restoreGame()
        if(resultado.text.toString() == result.toString()){
            if(round >= 5){

                endGame()
            }else{
                countDownTimer.cancel()
                round ++
                if(timeLeft <10 && timeLeft >=8){
                    score += 100
                    restoreGame()
                }
                if(timeLeft<8 && timeLeft >=5){
                    score += 50
                    restoreGame()
                }
                if(timeLeft<5 && timeLeft >=0){
                    score += 10
                    restoreGame()
                }
            }

        }else{
            if(round <= 5){
                gameRoundTextView.text = getString(R.string.round, round)
                endGame()
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment suma.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            suma().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
